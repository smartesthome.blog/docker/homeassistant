FROM homeassistant/raspberrypi3-homeassistant:0.99.3
VOLUME ["/config", "/var/db"]

COPY ./conf/configuration.yaml /config/configuration.yaml
COPY ./conf/secrets.yaml /config/secrets.yaml
COPY ./conf/subconf/* /config/subconf/

#Dependencies required for CalDAV integration
RUN apk add --no-cache libxml2-dev libxslt-dev zlib-dev

#Dependencies required for HomeKit
RUN apk add --no-cache avahi avahi-compat-libdns_sd avahi-dev

RUN pip3 install --no-cache-dir pymysql

#Ports 1: HomeAssistant, 2,3,4: HomeKit
EXPOSE 8123 5353 51826 51827
