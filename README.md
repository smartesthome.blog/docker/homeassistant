HomeAssistant Docker Image
==========================

1) Run this image with sensible defaults

```
docker run -v /home/pi/smartesthome/var/homeassistant:/config/ \
 -v /home/pi/smartesthome/var/homeassistant/db:/var/db \
 -v /home/pi/smartesthome/homeassistant/conf/configuration.yaml:/config/configuration.yaml:ro \
 -v /home/pi/smartesthome/homeassistant/conf/secrets.yaml:/config/secrets.yaml:ro \
 -v /home/pi/smartesthome/homeassistant/conf/subconf:/config/subconf:ro \
 -v /etc/localtime:/etc/localtime:ro \
 -v /etc/timezones:/etc/timezones:ro \
 -p 8080:8123
 iulius/homeassistant:snapshot
 ```